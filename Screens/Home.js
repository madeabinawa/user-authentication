/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, Text, Button } from 'react-native';

const Home = ({ logout }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{
                margin: 10,
                fontSize: 16,
                fontWeight: '300',
            }}>Welcome to Homepage</Text>
            <Button title="Logout" onPress={logout} />
        </View>
    );
};

export default Home;
