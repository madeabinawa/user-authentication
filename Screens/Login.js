/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */

import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';

const Login = ({ username, password, login }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.text}>You Haven't Login Yet</Text>
            <TextInput
                style={styles.textInput}
                placeholder="Username"
                onChangeText={username} />
            <TextInput
                style={styles.textInput}
                secureTextEntry={true}
                onChangeText={password}
                placeholder="Password" />
            <Button title="Login" onPress={login} />
        </View>
    );
};

const styles = StyleSheet.create({
    textInput: {
        borderColor: 'lightblue',
        borderStyle: 'solid',
        borderWidth: 2,
        borderRadius: 10,
        height: 40,
        width: 200,
        padding: 10,
        margin: 5,
    },
    text: {
        margin: 10,
        fontSize: 16,
        fontWeight: '300',
    },
});

export default Login;
