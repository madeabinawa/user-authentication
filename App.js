/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
import 'react-native-gesture-handler';
import React, { useEffect, useState, Alert } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';

import LoginScreen from './Screens/Login';
import HomeScreen from './Screens/Home';
import LoadScreen from './Screens/Loading';

const Stack = createStackNavigator();

const App = () => {
  const [foundToken, setFoundToken] = useState('');
  const [isLoad, setIsLoad] = useState(true);

  // Create state to store username and password input
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');



  const checkToken = async () => {
    try {
      let findingToken = await AsyncStorage.getItem('token');
      setFoundToken(findingToken);
      setIsLoad(false);
    } catch (error) {
      console.log(error);
    }
  };

  const isVerified = (usernameValue, passwordValue) => {
    // Use this credential to login
    let staticUsername = 'madeabinawa';
    let staticPassword = '12345';

    if (usernameValue === staticUsername && passwordValue === staticPassword) {
      return true;
    }
    return false;
  };

  const loginAction = async () => {
    if (isVerified(username, password)) {
      let dummyToken = 'CodeSeemToken';
      try {
        // write token to storage
        await AsyncStorage.setItem('token', dummyToken);
        setFoundToken('dummyToken');
      } catch (error) {
        console.log(error);
      }
    } else {
      alert('Username or Password is Incorrect!');
    }
  };

  const logoutAction = async () => {
    try {
      await AsyncStorage.removeItem('token');
      setFoundToken('');
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    checkToken();
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {
          foundToken ? <Stack.Screen name="Home">
            {props => <HomeScreen {...props} logout={logoutAction} />}
          </Stack.Screen>
            : (isLoad ? <Stack.Screen name="Load" options={{ HeaderShown: false }}>
              {props => <LoadScreen {...props} />}
            </Stack.Screen>
              : <Stack.Screen name="Login">
                {props => <LoginScreen {...props} username={(value) => setUsername(value)} password={(value) => setPassword(value)} login={loginAction} />}
              </Stack.Screen>
            )
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
